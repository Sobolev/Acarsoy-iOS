//
//  API.swift
//  Acarsoy
//
//  Created by Andrey Plygun on 02/06/22.
//

import Foundation
import Alamofire
import SwiftyJSON

struct UserDefaultsKeys {
    static let login = "login"
    static let password = "password"
    static let isLoggedIn = "isLoggedIn"
    static let token = "token"
}

class API {
    private static let baseURL = "https://acarsoyenerji.robosoftenerji.com/api/index.php?"
    private static let loginRoute = "operation=login&"
    
    static func loginWith(userName: String, password: String, callback: @escaping(_ success: Bool?, _ errorMsg: String?) -> ()) {
        let url = URL(string: baseURL + loginRoute + "email=\(userName)&password=\(password)")
        Alamofire.request(url!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).response { (response) in
            if let error = response.error {
                callback(nil, error.localizedDescription)
                return
            }
            if let data = response.data {
                let json = JSON(data)
                print(json)
                if let error = json["error"].int, error == 0 {
                    callback(true, nil)
                } else {
                    callback(nil, json["message"].string)
                }
            }
        }
    }
}
